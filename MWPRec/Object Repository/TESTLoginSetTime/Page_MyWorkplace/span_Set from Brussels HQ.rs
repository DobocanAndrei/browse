<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Set from Brussels HQ</name>
   <tag></tag>
   <elementGuidId>084ad815-e915-432d-9927-5a9fa1bf2c97</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='preferences-modal']/div/div/div[2]/uxmodalbody/form/ux-fieldset/div/div[2]/div/ux-form-group/div/div/div/div/div/ux-button/button/span</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>ux-button__content</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Set from Brussels HQ </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/app-root[1]/app-shell-new[1]/mywp-app-shell[1]/mywp-core-user-preferences[1]/ux-modal[@id=&quot;preferences-modal&quot;]/div[@id=&quot;preferences-modal&quot;]/div[@class=&quot;modal-dialog ux-modal__dialog&quot;]/div[@class=&quot;modal-content ux-modal__content&quot;]/div[@class=&quot;modal-body ux-modal__body&quot;]/uxmodalbody[1]/form[@class=&quot;ng-untouched ng-pristine ng-valid&quot;]/ux-fieldset[1]/div[@class=&quot;ux-fieldset mt-3&quot;]/div[@class=&quot;ux-fieldset__content&quot;]/div[1]/ux-form-group[1]/div[@class=&quot;ux-form-group form-group ux-form-group--vertical&quot;]/div[1]/div[@class=&quot;ux-form-group__input-content&quot;]/div[1]/div[@class=&quot;flex-container align-items-center&quot;]/ux-button[1]/button[@class=&quot;btn ux-button btn-outline-primary btn-sm ml-2 ux-button--with-label&quot;]/span[@class=&quot;ux-button__content&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <value>//div[@id='preferences-modal']/div/div/div[2]/uxmodalbody/form/ux-fieldset/div/div[2]/div/ux-form-group/div/div/div/div/div/ux-button/button/span</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[1]/following::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div/div/div/div/ux-button/button/span</value>
   </webElementXpaths>
</WebElementEntity>
