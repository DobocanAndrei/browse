<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Dashboard_sidebar-item__h</name>
   <tag></tag>
   <elementGuidId>bf80ffc3-4921-4be2-bea2-fb4d97ac895e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/preceding::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>sidebar-item__header-icon-wrapper-icon ion ion-ios-home-outline ux-icon-fw</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-shell-new[1]/mywp-app-shell[1]/div[@class=&quot;ux-app-shell language-en sidebar--close&quot;]/div[@class=&quot;ux-app-shell__sidebar&quot;]/div[@class=&quot;ux-app-shell__sidebar-body ps&quot;]/ux-layout-sidebar-items[1]/div[@class=&quot;sidebar-items&quot;]/ux-layout-sidebar-item[1]/div[@class=&quot;sidebar-item&quot;]/a[1]/div[@class=&quot;sidebar-item__header sidebar-item__header--large sidebar-item__header--is-toggling-subitems&quot;]/div[@class=&quot;sidebar-item__header-icon-wrapper&quot;]/span[@class=&quot;sidebar-item__header-icon-wrapper-icon ion ion-ios-home-outline ux-icon-fw&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Tasks'])[1]/preceding::span[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//a/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
