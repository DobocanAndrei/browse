<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>li_Sign out</name>
   <tag></tag>
   <elementGuidId>ae2d045c-ba42-42a3-8342-85dc0eaaad64</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='EN'])[1]/preceding::li[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>  Sign out </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-shell-new[1]/mywp-app-shell[1]/div[@class=&quot;ux-app-shell language-en sidebar--open&quot;]/div[@class=&quot;ux-app-shell__main&quot;]/header[@class=&quot;ux-app-shell__header&quot;]/div[@class=&quot;ux-app-shell__header-profile&quot;]/div[@class=&quot;profile-wrapper&quot;]/div[@class=&quot;profile-dropdown fx flipInY&quot;]/mywp-core-profile-menu[1]/ul[1]/li[7]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='EN'])[1]/preceding::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='English (en)'])[1]/preceding::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//li[7]</value>
   </webElementXpaths>
</WebElementEntity>
