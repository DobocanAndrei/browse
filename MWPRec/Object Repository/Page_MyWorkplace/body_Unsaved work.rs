<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>body_Unsaved work</name>
   <tag></tag>
   <elementGuidId>2eff037c-7779-4fa3-b1fa-15446f32f624</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//body</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>body</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>  


        
        
            
              
                
                  
                  
                      Unsaved work
                  
                  
                    ×
                  
                
                
                  
                
                    
                    
                        
    It seems that you have unsaved work. Do you want to go back to it? If you click &quot;No&quot; your work will be lost.

                    
                
            
                
                
                  
                      
                
                
                    
                        NO
                        YES
                    
                    
                
            
                  
                  
                
              
            
        
        
        
  
__Zone_enable_cross_context_check = true;</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//body</value>
   </webElementXpaths>
</WebElementEntity>
