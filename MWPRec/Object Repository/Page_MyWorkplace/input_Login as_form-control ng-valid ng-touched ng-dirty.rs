<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Login as_form-control ng-valid ng-touched ng-dirty</name>
   <tag></tag>
   <elementGuidId>1d5bb10a-17b1-4636-9e31-e849120adad8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Login as:'])[1]/following::input[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>form-control ng-valid ng-touched ng-dirty</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>display-property-name</name>
      <type>Main</type>
      <value>name</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>loading-text</name>
      <type>Main</type>
      <value>Fetching data...</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>max-num-list</name>
      <type>Main</type>
      <value>5</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>min-chars</name>
      <type>Main</type>
      <value>2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>no-match-found-text</name>
      <type>Main</type>
      <value>No Match Found</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>placeholder</name>
      <type>Main</type>
      <value>Enter user properties</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-shell-new[1]/mywp-app-shell[1]/div[@class=&quot;ux-app-shell language-en sidebar--open&quot;]/div[@class=&quot;ux-app-shell__main&quot;]/header[@class=&quot;ux-app-shell__header&quot;]/div[@class=&quot;ux-app-shell__header-profile&quot;]/div[@class=&quot;profile-wrapper&quot;]/div[@class=&quot;profile-dropdown fx flipInY&quot;]/mywp-core-profile-menu[1]/ul[1]/li[@class=&quot;ux-u-bg-color-grey-lightest d-block&quot;]/div[@class=&quot;input-group&quot;]/mywp-shared-user-picker[1]/div[@class=&quot;ux-clearable-input-wrapper&quot;]/div[@class=&quot;ngui-auto-complete-wrapper&quot;]/input[@class=&quot;form-control ng-valid ng-touched ng-dirty&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login as:'])[1]/following::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='×'])[1]/preceding::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Sign in'])[1]/preceding::input[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//input</value>
   </webElementXpaths>
</WebElementEntity>
