<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>span_Filters</name>
   <tag></tag>
   <elementGuidId>f914663c-7fcd-4d53-8299-d10adacda463</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced filter'])[1]/preceding::span[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>span</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value> Filters </value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[1]/app-root[1]/app-shell-new[1]/mywp-app-shell[1]/div[@class=&quot;ux-app-shell language-en sidebar--close&quot;]/div[@class=&quot;ux-app-shell__main&quot;]/div[@class=&quot;ux-app-shell__main-content&quot;]/taskmgr-main-ct[1]/taskmgr-main-cp[1]/div[@class=&quot;container-fluid&quot;]/ux-filter-results[1]/div[@class=&quot;ux-filter-results&quot;]/div[@class=&quot;ux-filter-results__filter&quot;]/ux-panel[1]/div[@class=&quot;ux-panel ux-panel--is-collapsible&quot;]/div[@class=&quot;ux-panel-item  ux-panel-item--flat&quot;]/div[@class=&quot;ux-panel-header&quot;]/ux-a-label[1]/div[@class=&quot;ux-a-label ux-a-label--t-&quot;]/div[@class=&quot;ux-a-label__content&quot;]/div[@class=&quot;ux-a-label__label&quot;]/span[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Advanced filter'])[1]/preceding::span[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Navigation'])[1]/preceding::span[3]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <value>//div[2]/div/ux-a-label/div/div/div/span</value>
   </webElementXpaths>
</WebElementEntity>
