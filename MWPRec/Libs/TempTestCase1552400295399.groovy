import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('D:\\Users\\dobocan\\AppData\\Local\\Temp\\1\\Katalon\\20190312_151815\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runWSVerificationScript(new TestCaseBinding('',[:]), 'import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI\nimport com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile\nimport com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW\nimport com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS\nimport static com.kms.katalon.core.testobject.ObjectRepository.findTestObject\nimport static com.kms.katalon.core.testdata.TestDataFactory.findTestData\nimport static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase\nimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint\nimport com.kms.katalon.core.model.FailureHandling as FailureHandling\nimport com.kms.katalon.core.testcase.TestCase as TestCase\nimport com.kms.katalon.core.testdata.TestData as TestData\nimport com.kms.katalon.core.testobject.TestObject as TestObject\nimport com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint\nimport internal.GlobalVariable as GlobalVariable\nimport org.openqa.selenium.Keys as Keys\n\nWebUI.openBrowser(\'\')\n\nWebUI.navigateToUrl(\'https://ecas.acceptance.ec.europa.eu/cas/oauth2/authorize?client_id=akM9Tp6geXhrfpj43EXszkBlDDzJ4jQagL7dbQq715wYVhTGbubHm8zWeicK9CJ883B4QCiYL11KLHVoc2SbhiG-10m5aNqpSIShTDSYgU1Te0&redirect_uri=https%3A%2F%2Fmwp.tst.ecdevops.eu&response_type=id_token&scope=openid%20email%20profile&state=79920f5d6596439185653bd6e0e41304&nonce=127abcaf591b46dd91df513f92e69c80\')\n\nWebUI.setText(findTestObject(\'Page_EU Login/input_e-mail address_username\'), \'w0501060\')\n\nWebUI.click(findTestObject(\'Page_EU Login/input_e-mail address_whoamiSubmit\'))\n\nWebUI.setEncryptedText(findTestObject(\'Page_Login/input_Password_password\'), \'rOjXA6btYeQHxbQQnd0htQ==\')\n\nWebUI.click(findTestObject(\'Page_Login/input_DigiPass serial number__submit\'))\n\nWebUI.waitForElementVisible(findTestObject(\'Page_MyWorkplace/div_Default Dashboard\'), 0)\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/strong_Generic User w0501060\'))\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/div_My tasks\'), FailureHandling.STOP_ON_FAILURE)\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/div_Portfolio\'))\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/div_Process Centre\'))\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/span_Timezones\'))\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/span_My portfolio\'))\n\nWebUI.verifyElementVisible(findTestObject(\'Page_MyWorkplace/span_My Portfolio status\'))\n\nWebUI.closeBrowser()\n\n', FailureHandling.STOP_ON_FAILURE, true)

