import com.kms.katalon.core.main.TestCaseMain
import com.kms.katalon.core.logging.KeywordLogger
import com.kms.katalon.core.testcase.TestCaseBinding
import com.kms.katalon.core.driver.internal.DriverCleanerCollector
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.webui.contribution.WebUiDriverCleaner
import com.kms.katalon.core.mobile.contribution.MobileDriverCleaner
import com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner


DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.webui.contribution.WebUiDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.mobile.contribution.MobileDriverCleaner())
DriverCleanerCollector.getInstance().addDriverCleaner(new com.kms.katalon.core.cucumber.keyword.internal.CucumberDriverCleaner())


RunConfiguration.setExecutionSettingFile('D:\\Users\\dobocan\\AppData\\Local\\Temp\\1\\Katalon\\20190312_154045\\execution.properties')

TestCaseMain.beforeStart()

        TestCaseMain.runWSVerificationScript(new TestCaseBinding('',[:]), 'import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI\nimport com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile\nimport com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW\nimport com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS\nimport static com.kms.katalon.core.testobject.ObjectRepository.findTestObject\nimport static com.kms.katalon.core.testdata.TestDataFactory.findTestData\nimport static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase\nimport static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint\nimport com.kms.katalon.core.model.FailureHandling as FailureHandling\nimport com.kms.katalon.core.testcase.TestCase as TestCase\nimport com.kms.katalon.core.testdata.TestData as TestData\nimport com.kms.katalon.core.testobject.TestObject as TestObject\nimport com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint\nimport internal.GlobalVariable as GlobalVariable\nimport org.openqa.selenium.Keys as Keys\n\nWebUI.openBrowser(\'\')\n\nWebUI.navigateToUrl(\'https://mwp.tst.ecdevops.eu/mwp\')\n\nWebUI.setText(findTestObject(\'Page_EU Login/input_e-mail address_username\'), \'w0501060\')\n\nWebUI.click(findTestObject(\'Page_EU Login/input_e-mail address_whoamiSubmit\'))\n\nWebUI.setEncryptedText(findTestObject(\'Page_Login/input_Password_password\'), \'rOjXA6btYeQHxbQQnd0htQ==\')\n\nWebUI.click(findTestObject(\'Page_Login/input_DigiPass serial number__submit\'))\n\nWebUI.click(findTestObject(\'Page_MyWorkplace/span_Generic User w0501060_ux-icon ux-icon-angle-down\'))\n\nWebUI.click(findTestObject(\'Page_MyWorkplace/li_Sign out\'))\n\nWebUI.click(findTestObject(\'Page_Logout/input_Logout_refuse\'))\n\nWebUI.navigateToUrl(\'https://mwp.tst.ecdevops.eu/mwp/home\')\n\nWebUI.click(findTestObject(\'Page_MyWorkplace/span_Generic User w0501060_ux-icon ux-icon-angle-down\'))\n\nWebUI.click(findTestObject(\'Page_MyWorkplace/li_Sign out\'))\n\nWebUI.click(findTestObject(\'Page_Logout/input_Logout_proceed\'))\n\nWebUI.closeBrowser()\n\n', FailureHandling.STOP_ON_FAILURE, true)

