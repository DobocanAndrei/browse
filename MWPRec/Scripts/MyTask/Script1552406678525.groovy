import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

'Maximize current window'
WebUI.maximizeWindow()

WebUI.navigateToUrl('https://ecas.acceptance.ec.europa.eu/cas/oauth2/authorize?client_id=akM9Tp6geXhrfpj43EXszkBlDDzJ4jQagL7dbQq715wYVhTGbubHm8zWeicK9CJ883B4QCiYL11KLHVoc2SbhiG-10m5aNqpSIShTDSYgU1Te0&redirect_uri=https%3A%2F%2Fmwp.tst.ecdevops.eu&response_type=id_token&scope=openid%20email%20profile&state=4234e67bdd9c43bfb87e0978fa6c8c50&nonce=c4648c9266814fbc998486cb4faccea5')

WebUI.setText(findTestObject('Page_EU Login/input_e-mail address_username (4)'), 'w0501060')

WebUI.click(findTestObject('Page_EU Login/input_e-mail address_whoamiSubmit (4)'))

WebUI.setEncryptedText(findTestObject('Page_Login/input_Password_password (4)'), 'rOjXA6btYeQHxbQQnd0htQ==')

WebUI.click(findTestObject('Page_Login/input_DigiPass serial number__submit (4)'))

WebUI.click(findTestObject('Page_MyWorkplace/div_My tasks'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_MyWorkplace/button_Retry'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_MyWorkplace/span_Filters'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_MyWorkplace/h6_Navigation'), 0)

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_MyWorkplace/h6_Advanced filter'), 0)

WebUI.closeBrowser()

