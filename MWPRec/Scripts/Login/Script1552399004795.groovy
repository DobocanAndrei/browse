import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://ecas.acceptance.ec.europa.eu/cas/oauth2/authorize?client_id=akM9Tp6geXhrfpj43EXszkBlDDzJ4jQagL7dbQq715wYVhTGbubHm8zWeicK9CJ883B4QCiYL11KLHVoc2SbhiG-10m5aNqpSIShTDSYgU1Te0&redirect_uri=https%3A%2F%2Fmwp.tst.ecdevops.eu&response_type=id_token&scope=openid%20email%20profile&state=eed73f7ce78a4e8c8bc1560aff3cc5bc&nonce=a0079228255b4818b1c204c3ec1bf8bb')

WebUI.setText(findTestObject('Object Repository/Page_EU Login/input_e-mail address_username'), 'w0501060')

WebUI.click(findTestObject('Object Repository/Page_EU Login/input_e-mail address_whoamiSubmit'))

WebUI.setEncryptedText(findTestObject('Object Repository/Page_Login/input_Password_password'), 'rOjXA6btYeQHxbQQnd0htQ==')

WebUI.click(findTestObject('Object Repository/Page_Login/input_DigiPass serial number__submit'))

WebUI.closeBrowser()

