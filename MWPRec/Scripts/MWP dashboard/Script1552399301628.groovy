import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://ecas.acceptance.ec.europa.eu/cas/oauth2/authorize?client_id=akM9Tp6geXhrfpj43EXszkBlDDzJ4jQagL7dbQq715wYVhTGbubHm8zWeicK9CJ883B4QCiYL11KLHVoc2SbhiG-10m5aNqpSIShTDSYgU1Te0&redirect_uri=https%3A%2F%2Fmwp.tst.ecdevops.eu&response_type=id_token&scope=openid%20email%20profile&state=79920f5d6596439185653bd6e0e41304&nonce=127abcaf591b46dd91df513f92e69c80')

WebUI.setText(findTestObject('Page_EU Login/input_e-mail address_username (1)'), 'w0501060')

WebUI.click(findTestObject('Page_EU Login/input_e-mail address_whoamiSubmit (1)'))

WebUI.setEncryptedText(findTestObject('Page_Login/input_Password_password (1)'), 'rOjXA6btYeQHxbQQnd0htQ==')

WebUI.click(findTestObject('Page_Login/input_DigiPass serial number__submit (1)'))

WebUI.waitForElementVisible(findTestObject('Page_MyWorkplace/div_Default Dashboard'), 0)

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/strong_Generic User w0501060'))

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/div_My tasks'), FailureHandling.STOP_ON_FAILURE)

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/div_Portfolio'))

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/div_Process Centre'))

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/span_Timezones'))

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/span_My portfolio (1)'))

WebUI.verifyElementVisible(findTestObject('Page_MyWorkplace/span_My Portfolio status (1)'))

WebUI.closeBrowser()

