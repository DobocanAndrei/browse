import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

WebUI.openBrowser('')

WebUI.navigateToUrl('https://mwp.tst.ecdevops.eu/mwp')

WebUI.setText(findTestObject('Page_EU Login/input_e-mail address_username (2)'), 'w0501060')

WebUI.click(findTestObject('Page_EU Login/input_e-mail address_whoamiSubmit (2)'))

WebUI.setEncryptedText(findTestObject('Page_Login/input_Password_password (2)'), 'rOjXA6btYeQHxbQQnd0htQ==')

WebUI.click(findTestObject('Page_Login/input_DigiPass serial number__submit (2)'))

WebUI.click(findTestObject('Object Repository/Page_MyWorkplace/span_Generic User w0501060_ux-icon ux-icon-angle-down'))

WebUI.click(findTestObject('Object Repository/Page_MyWorkplace/li_Sign out'))

WebUI.click(findTestObject('Object Repository/Page_Logout/input_Logout_refuse'))

WebUI.navigateToUrl('https://mwp.tst.ecdevops.eu/mwp/home')

WebUI.click(findTestObject('Object Repository/Page_MyWorkplace/span_Generic User w0501060_ux-icon ux-icon-angle-down'))

WebUI.click(findTestObject('Object Repository/Page_MyWorkplace/li_Sign out'))

WebUI.click(findTestObject('Object Repository/Page_Logout/input_Logout_proceed'))

WebUI.closeBrowser()

